#!/usr/bin/python

#########################################################################
#
# Copyright (C) 2015 Stefano Menegon ste.menegon@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
#########################################################################

import sys
import os
import urllib
import tempfile
import shutil
import fiona
import subprocess
import psycopg2
from psycopg2.extensions import AsIs

#########################################################################
#
# configuration / settings
#
#########################################################################

# Database configuration
dbconf = {
    'database': 'exercise1',
    'user': 'postgres',
    'password': 'password',
    'host': 'localhost',
    'port': 5432
}

# Path of the directory to save the daily hotspots for countries
exportdir = "/tmp/shapefiles"

# Shapefiles of MODIS active fire data for the last 24 hours
global24_url = "https://firms.modaps.eosdis.nasa.gov/active_fire/shapes/zips/Global_24h.zip"

# Layer name contained within the Global_24h.zip
global24_layername = 'Global_24h'

# PostGIS table name
table_name = 'hotspot'


class ActiveFireDB(object):

    """Helper class to interact with postgis DB.
    """
    def __init__(self, dbconf, table_name):
        self.dbconf = dbconf
        self.table_name = table_name
        self.conn = self.get_connection()

    def get_connection(self):
        connection = psycopg2.connect(**self.dbconf)
        return connection

    def check_table(self):
        cur = self.conn.cursor()
        cur.execute("select exists(select 1 from information_schema.tables where table_name=%s)",
                    (self.table_name,))
        res = cur.fetchone()[0]
        cur.close()
        return res

    def set_srid(self, srid):
        cur = self.conn.cursor()
        cur.execute("select UpdateGeometrySRID(%s,'wkb_geometry', 4326);",
                    (self.table_name,))
        self.conn.commit()
        cur.close()
        return True

    def add_country_column(self):
        cur = self.conn.cursor()
        cur.execute("alter table %s add column cntry_iso3 varchar(3)",
                    (AsIs(self.table_name),))
        self.conn.commit()
        cur.close()
        return True

    def add_lastimport_column(self):
        cur = self.conn.cursor()
        cur.execute("alter table %s add column lastimport boolean",
                    (AsIs(self.table_name),))
        self.conn.commit()
        cur.close()
        return True

    def update_country_column(self):
        cur = self.conn.cursor()
        sql = """
        update %s set cntry_iso3 = c.iso3 from country c
        where cntry_iso3 is null
        and st_within(%s.wkb_geometry, c.wkb_geometry) """
        cur.execute(sql, (AsIs(self.table_name), AsIs(self.table_name)))
        self.conn.commit()
        cur.close()
        return True

    def update_lastimport(self):
        """
        Set last value
        """
        cur = self.conn.cursor()
        sql = """update %s set lastimport = case when lastimport then 'f'::bool else
        't'::bool end where lastimport or lastimport is null"""
        cur.execute(sql, (AsIs(self.table_name), ))
        self.conn.commit()
        cur.close()
        return True

    def import_data(self, tmp_file):
        pg = self.dbconf.copy()
        dbname = pg.pop('database')
        pg_string = ' '.join(['{}={}'.format(k,v) for k,v in pg.iteritems()])
        pg_string = 'PG:dbname={} {}'.format(dbname, pg_string)
        cmd = ["ogr2ogr",
               "-append",
               "-f", "PostgreSQL",
               pg_string,
               '/vsizip/{}'.format(tmp_file),
               '-nln',
               self.table_name]
        # remove append mode if table doesn't exist
        append = self.check_table()
        if not append:
            cmd.remove('-append')
        subprocess.check_call(cmd)

    def get_countries(self):
        cur = self.conn.cursor()
        cur.execute("select distinct cntry_iso3 from hotspot where cntry_iso3 is not null")
        res = cur.fetchall()
        cur.close()
        return res

    def load(self, tmp_file):
        append = self.check_table()
        self.import_data(tmp_file)
        if not append:
            self.set_srid(4326)
            self.add_country_column()
            self.add_lastimport_column()
        self.update_country_column()
        self.update_lastimport()

    def export(self, exportdir):
        countries = self.get_countries()
        for c in countries:
            iso3 = c[0]
            filter = "cntry_iso3='{}' and lastimport".format(iso3)
            filepath = os.path.join(exportdir, "{}.shp".format(iso3))
            self.export_shpfiles(filter, filepath)

    def export_shpfiles(self, filter, filepath):
        pg = self.dbconf.copy()
        dbname = pg.pop('database')
        pg_string = ' '.join(['{}={}'.format(k,v) for k,v in pg.iteritems()])
        pg_string = 'PG:dbname={} {}'.format(dbname, pg_string)
        cmd = ["ogr2ogr",
               "-where",
               filter,
               filepath,
               pg_string,
               self.table_name
           ]
        return subprocess.check_call(cmd)


def check_shp(tmp_file, layer_name):
    """
    Check if the shapefile contains the right layer
    """
    with fiona.drivers():
        layers = fiona.listlayers('/', vfs='zip://{}'.format(tmp_file))
        if layer_name not in layers:
            raise ValueError("The archive doesn't contain the file", global24_layername)


def load_activefire():
    """
    Performs the upload action
    """
    try:
        # create a temp dir
        tmp_dir = tempfile.mkdtemp()
        tmp_file = os.path.join(tmp_dir, "download.zip")
        # download the zip file in the temp_dir
        urllib.urlretrieve(global24_url,
                           tmp_file)
        check_shp(tmp_file, global24_layername)
        db = ActiveFireDB(dbconf, table_name)
        db.load(tmp_file)
    finally:
        # delete temp dir
        try:
            shutil.rmtree(tmp_dir)
        except OSError as exc:
            if exc.errno != errno.ENOENT:
                raise # re-raise exception


def export_activefire():
    """
    Performs the export action
    """
    db = ActiveFireDB(dbconf, table_name)
    db.export(exportdir)


if __name__ == '__main__':
    if len(sys.argv) != 2 or sys.argv[1] not in ('load', 'export'):
        sys.stderr.write("Invalid action\n")
        sys.exit("Usage: {} <load|export>".format(sys.argv[0]))
    action = sys.argv[1]
    if action == 'load':
        load_activefire()
    elif action == 'export':
        export_activefire()
