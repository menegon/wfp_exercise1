# WFP Exercise 1 - ActiveFire #

This repo contains the script activefire.py.
The script performs two actions:

* load:
    1. download the Active Fire for the last 24 hours
    1. check the integrity of the file
    1. check the DB connection
    1. import the zip file using an ogr2ogr: if a table is already present the script add the '-append' option
    1. add two new columns (only for the first import):
        * cntry_iso3
        * lastimport
    1. perform a geometric intersection to extract the countries ISO3 for each hotspot
    1. set the lastimport to mark the lasted imported records

* export:
    1. export the daily hotspots (only the hotspots marked as "lastimport") for each country

## Configuration
You can configure activefire.py editing the configuration section within the file itself.


    # Database configuration
    dbconf = {
        'database': 'exercise1',
        'user': 'postgres',
        'password': 'password',
        'host': 'localhost',
        'port': 5432
    }
    
    # Path of the directory to save the daily hotspots for countries
    exportdir = "/tmp/shapefiles"
    
    # Shapefiles of MODIS active fire data for the last 24 hours
    global24_url = "https://firms.modaps.eosdis.nasa.gov/active_fire/shapes/zips/Global_24h.zip"
    
    # Layer name contained within the Global_24h.zip
    global24_layername = 'Global_24h'

    # PostGIS table name
    table_name = 'hotspot'

## Requirements
activefire.py requires Fiona  https://pypi.python.org/pypi/Fiona

## Usage
Just run the script specifying the action (load or export).

    ./activefire.py <load|export>